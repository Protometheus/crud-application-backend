package com.genergy.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudTestApplicaionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudTestApplicaionApplication.class, args);
	}

}
