package com.genergy.edu.entity;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("UserInfo")
public class UserInfo {

	private String userId;
	private String userPw;
	private String email;
	private String tel;
	private String address;
	
}
