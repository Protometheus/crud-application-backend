package com.genergy.edu.store.logic;

import org.springframework.beans.factory.annotation.Autowired;

import com.genergy.edu.entity.UserInfo;
import com.genergy.edu.store.UserInfoStore;
import com.genergy.edu.store.mapper.UserInfoMapper;

public class UserInfoStoreLogic implements UserInfoStore {

	@Autowired
	UserInfoMapper userInfoMapper;
	
	@Override
	public int insert(UserInfo userInfo) {
		return userInfoMapper.insert(userInfo);
	}

}
