package com.genergy.edu.store.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.genergy.edu.entity.UserInfo;

@Repository
@Mapper
public interface UserInfoMapper {
	
	public int insert(UserInfo userInfo);
	public int update(UserInfo userInfo);
	public int delete(String userId);
	public UserInfo select(String userId);
	public List<UserInfo> selectAll();
	
}
