package com.genergy.edu.store;

import com.genergy.edu.entity.UserInfo;

public interface UserInfoStore {

	public int insert(UserInfo userInfo);

}
