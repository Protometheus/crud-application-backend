package com.genergy.edu.service;

import java.util.List;

import com.genergy.edu.entity.UserInfo;

public interface UserInfoService {

	public String add(UserInfo userInfo);
	public String modify(UserInfo userInfo);
	public String remove(String userId);
	public UserInfo retrieve(String userId);
	public List<UserInfo> retrieveAll();
	
}
