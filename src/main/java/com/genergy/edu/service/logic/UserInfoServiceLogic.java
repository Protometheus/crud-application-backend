package com.genergy.edu.service.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genergy.edu.entity.UserInfo;
import com.genergy.edu.service.UserInfoService;
import com.genergy.edu.store.mapper.UserInfoMapper;

@Service
public class UserInfoServiceLogic implements UserInfoService {
	
	@Autowired
	private UserInfoMapper userInfoMapper;
	
	@Override
	public String add(UserInfo userInfo) {
		return (userInfoMapper.insert(userInfo) == 1) ? "success" : "fail";
	}

	@Override
	public String modify(UserInfo userInfo) {
		return (userInfoMapper.update(userInfo) == 1) ? "success" : "fail";
	}

	@Override
	public String remove(String userId) {
		return (userInfoMapper.delete(userId) == 1) ? "success" : "fail";
	}

	@Override
	public UserInfo retrieve(String userId) {
		return userInfoMapper.select(userId);
	}

	@Override
	public List<UserInfo> retrieveAll() {
		return userInfoMapper.selectAll();
	}
	
}
